//
// Created by SERGEY SUVOROV on 20/04/2022.
//

#include "TFile.h"
#include "TTree.h"

#include <vector>

#include "TRawEvent.hxx"

int main() {
    TFile file("path_to_root.root", "READ");
    auto tree = file.Get<TTree>("EventTree");
    auto event = new TRawEvent();
    tree->SetBranchAddress("TRawEvent", &event);

    // loop over events in the tree
    for (uint i = 0; i < tree->GetEntries(); ++i) {
        tree->GetEntry(i);

        // loop over hits in event
        std::cout << "Event " << event->GetID() << " with " << event->GetHits().size() << " hits\n";
        for (const auto& hit : event->GetHits()) {
            std::cout << "Hit starts at t = " << hit->GetTime() << " and is " << hit->GetADCvector().size() << " bins long\n";
        }
    }
}
