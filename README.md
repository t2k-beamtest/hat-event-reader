# HAT event reader

Example project for reading of TRawEvent from [hat_event](https://gitlab.com/t2k-beamtest/hat_event)

## Compile

```bash
git submodule update --init --recursive
mkdir build; cd build;
cmake ../
make
```

## Run

```bash
./Test.exe 
```
